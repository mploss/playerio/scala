package com.playerio

import java.net.URL

import cats.effect.Sync
import cats.free.Free
import com.playerio.api.Api
import com.playerio.message.PlayerIOMessage
import hammock._
import hammock.marshalling._
import hammock.hi._
import implicits._
import scalapb.{GeneratedMessage, Message}

case class Channel[F[_]: Sync: InterpTrans](endpoint: Uri, token: Option[String] = None) {

  def call[I <: GeneratedMessage with Message[I], O <: GeneratedMessage with Message[O]](
    args: I
  )(implicit message: PlayerIOMessage.Aux[I, O]): F[O] =
    Api[F](endpoint / "api", endpoint / "json").call(message.code, args, token)

}
