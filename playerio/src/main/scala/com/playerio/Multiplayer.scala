package com.playerio

import cats._
import cats.implicits._
import cats.effect._
import com.playerio.proto.room.{JoinRoomArgs, ListRoomsArgs, ListRoomsOutput}
import com.playerio.proto.struct.{RoomInfo, ServerEndpoint}
import implicits._
import fs2._

case class Multiplayer[F[_]: Functor](pio: Channel[F]) {

  def list(
    roomType: String,
    resultLimit: Int = 50,
    resultOffset: Int = 0,
    searchCriteria: Map[String, String] = Map(),
    onlyDevRooms: Boolean = false
  ): F[List[RoomInfo]] =
    pio.call(ListRoomsArgs(roomType, searchCriteria, resultLimit, resultOffset, onlyDevRooms)).map(_.rooms.toList)

  def join(room: RoomInfo, joinData: Map[String, String] = Map())(
    implicit cce: ConcurrentEffect[F],
    cs: ContextShift[F]
  ): Resource[F, Connection[F]] = { // TODO development server support
    val frfa: F[Resource[F, Connection[F]]] = pio.call(JoinRoomArgs(room.id, joinData)).map { jro =>
      val key = jro.joinKey
      val joiner = Connection[F](key)(_, _)
      val endpoints = jro.endpoints.toList
      val selectWorking = Stream
        .emits(endpoints)
        .flatMap {
          case ServerEndpoint(url, port) => Stream.resource(joiner(url, port)).attempt
        }
        .takeThrough(_.isRight)
        .last
      val noOpt: Resource[F, Either[Throwable, Connection[F]]] = selectWorking.compile.resource.lastOrError
        .map(_.getOrElse(Left(new IllegalStateException("No valid endpoints to connect to!"))))
      noOpt.rethrow
    }
    Resource.suspend(frfa)
  }

}
