package com.playerio

import cats.Functor
import com.playerio.proto.struct.PlayerInsightState

case class Client[F[_]: Functor](
  pio: Channel[F],
  gameId: String,
  userId: String,
  gameFSRedirectMap: String,
  playerInsightState: Option[PlayerInsightState]
) {

  val multiplayer: Multiplayer[F] = Multiplayer(pio)

}
