package com.playerio

import cats._
import cats.implicits._
import cats.effect._
import com.playerio.proto.auth._
import hammock._
import implicits._

sealed trait PlayerIO {

  val token: Option[String]

  def channel[F[_]: Sync: InterpTrans]: Channel[F] =
    Channel[F](uri"https://api.playerio.com/api", token)

  def authenticate[F[_]: Sync: InterpTrans](
    gameId: String,
    connectionId: String,
    authenticationArguments: Map[String, String],
    playerInsightSegments: List[String]
  ): F[Client[F]] = {
    channel
      .call(
        AuthenticateArgs(gameId, connectionId, authenticationArguments, playerInsightSegments, "scala", Map(), List())
      )
      .map { ao =>
        val newPio = new TokenedPlayerIO(ao.token)
        val cli = Client[F](newPio.channel, gameId, ao.userId, ao.gameFsRedirectMap, ao.playerInsightState)
        // TODO handle playcodes
        cli
      }
  }

}

private class TokenedPlayerIO(t: String) extends PlayerIO {
  override val token: Option[String] = Some(t)
}

object PlayerIO extends PlayerIO {
  override val token: Option[String] = None
}
