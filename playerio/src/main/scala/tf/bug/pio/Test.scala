package tf.bug.pio

import cats._
import cats.implicits._
import cats.effect._
import com.playerio.PlayerIO
import hammock.InterpTrans
import com.playerio.api.implicits._

object Test extends IOApp {

  override def run(args: List[String]): IO[ExitCode] = program[IO].as(ExitCode.Success)

  def program[F[_]: ConcurrentEffect: ContextShift]: F[Unit] = {
    val connectionF = for {
      client <- PlayerIO.authenticate("kittygame-48om7qu7teeazkf9gana", "public", Map("userId" -> "dab123"), List())
      rooms <- client.multiplayer.list("KittyRpg2")
    } yield client.multiplayer.join(rooms.head)
    connectionF.flatMap { res =>
      res.use { conn =>
        val ps = conn.reads.evalMap(c => Sync[F].delay(println(c)))
        ps.compile.drain
      }
    }
  }

}
