package com.playerio.api

import cats.data.EitherK
import cats.effect.Sync
import cats.free.Free
import com.playerio.message.{DataPacker, MessageData, PlayerIOMessage}
import hammock.marshalling.MarshallF
import hammock._
import implicits._
import scalapb.{GeneratedMessage, Message}

case class Api[F[_]: Sync: InterpTrans](protobufEndpoint: Uri, jsonEndpoint: Uri) {

  def call[I <: GeneratedMessage with Message[I], O <: GeneratedMessage with Message[O]](
    method: Int,
    message: I,
    token: Option[String] = None
  )(implicit ev: PlayerIOMessage.Aux[I, O]): F[O] = {
    val data: MessageData = DataPacker.pack(message)
    val req = Hammock.request(
      Method.POST,
      EndpointSelector(protobufEndpoint, jsonEndpoint) / method.toString,
      token.fold(Map.empty[String, String])(t => Map("playertoken" -> t)),
      Some(message)
    )
    req.as[O].exec[F]
  }

}
