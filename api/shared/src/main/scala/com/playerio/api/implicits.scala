package com.playerio.api

import cats.effect.Async
import com.playerio.message.PlayerIOMessage
import hammock.Entity.ByteArrayEntity
import hammock.{CodecException, Decoder, Encoder, Entity, InterpTrans}
import scalapb.{GeneratedMessage, Message}

object implicits {

  implicit def interp[F[_]: Async]: InterpTrans[F] = InterpProvider[F]

  implicit def messageEncoder[M <: GeneratedMessage with Message[M]](implicit message: PlayerIOMessage[M]): Encoder[M] =
    (a: M) => EntityTransformer(message.encode(a))

  implicit def messageDecoder[M <: GeneratedMessage with Message[M]](
    implicit message: PlayerIOMessage.Aux[_, M]
  ): Decoder[M] = { // TODO Handle new tokens
    case EntityTransformer(eths) => Right(message.decode(eths))
    case a => Left(CodecException.withMessage(s"Could not decode entity: $a"))
  }

}
