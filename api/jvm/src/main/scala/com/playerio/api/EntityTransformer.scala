package com.playerio.api

import com.playerio.message.MessageData
import hammock.Entity
import hammock.Entity.ByteArrayEntity

object EntityTransformer {

  def apply(d: MessageData): Entity = ByteArrayEntity(d)

  def unapply(arg: Entity): Option[MessageData] = arg.cata(
    _ => None,
    b => Some(b.body),
    _ => None
  )

}
