package com.playerio.api

import cats.effect.Async
import hammock.InterpTrans
import hammock.js.Interpreter

object InterpProvider {

  def apply[F[_]: Async]: InterpTrans[F] = Interpreter.instance[F]

}
