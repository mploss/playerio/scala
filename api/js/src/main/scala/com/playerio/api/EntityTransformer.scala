package com.playerio.api

import com.playerio.message.MessageData
import hammock.Entity
import hammock.Entity.StringEntity
import io.circe.parser._

import scala.scalajs.niocharset.StandardCharsets
import scala.util.Try

object EntityTransformer {

  def apply(d: MessageData): Entity = StringEntity(d.noSpaces)

  def unapply(arg: Entity): Option[MessageData] = arg.cata(
    s => parse(s.body).toOption,
    b => Try(new String(b.body, StandardCharsets.UTF_8)).toOption.flatMap(parse(_).toOption),
    _ => None
  )

}
