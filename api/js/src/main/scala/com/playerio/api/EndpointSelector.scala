package com.playerio.api

import hammock.Uri

object EndpointSelector {

  def apply(proto: Uri, json: Uri): Uri = json

}
