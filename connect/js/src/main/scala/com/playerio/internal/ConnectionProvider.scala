package com.playerio.internal

import java.nio.ByteBuffer

import cats.{Applicative, MonadError}
import cats.effect.{Async, ConcurrentEffect, ContextShift, IO, Resource, Sync}
import com.playerio.Connection
import fs2._
import fs2.concurrent.Queue
import org.scalajs.dom
import org.scalajs.dom.MessageEvent
import org.scalajs.dom.raw.{Blob, FileReader}
import spire.math.UByte

import scala.scalajs.js.typedarray._
import scala.util.Try

object ConnectionProvider {

  def socket[F[_]: ConcurrentEffect: ContextShift](endpoint: String, port: Int): Resource[F, Connection[F]] = {
    Resource
      .make(Async[F].async[dom.WebSocket] { cb =>
        Try {
          val ws = new dom.WebSocket(s"wss://$endpoint:$port/")
          ws.onopen = _ => cb(Right(ws))
        }.fold(t => cb(Left(t)), identity)
      })(
        ws =>
          Async[F].async[Unit] { cb =>
            ws.onclose = _ => cb(Right(()))
            ws.close()
        }
      )
      .map { ws =>
        val readEvents = for {
          q <- Stream.eval(Queue.unbounded[F, MessageEvent])
          _ <- Stream.eval {
            Sync[F].delay(ws.onmessage = e => ConcurrentEffect[F].runAsync(q.enqueue1(e))(_ => IO.unit).unsafeRunSync())
          }
          m <- q.dequeue
        } yield m.data
        val reads: Stream[F, Chunk[UByte]] = readEvents.evalMap {
          case s: String => Applicative[F].pure(s.getBytes("UTF-8"))
          case b: Blob =>
            val reader = new FileReader
            val af = Async[F].async[Array[Byte]] { callback =>
              reader.onloadend = _ => {
                val res = reader.result
                val asBuf = res.asInstanceOf[ArrayBuffer]
                val asBB = TypedArrayBuffer.wrap(asBuf)
                val arr = asBB.array()
                callback(Right(arr))
              }
            }
            reader.readAsArrayBuffer(b)
            af
          case a: ArrayBuffer =>
            val asBB = TypedArrayBuffer.wrap(a)
            Applicative[F].pure(asBB.array())
        }.map(arr => Chunk(arr.map(UByte(_)): _*))
        val writes: Pipe[F, Chunk[UByte], Unit] = { in =>
          in.evalMap { b =>
            Sync[F].delay(ws.send(b.map(_.signed).toArray.toTypedArray.buffer))
          }
        }
        Connection(reads, writes)
      }
  }

}
