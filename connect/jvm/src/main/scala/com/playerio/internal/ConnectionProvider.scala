package com.playerio.internal

import java.net.InetSocketAddress
import java.nio.channels.AsynchronousChannelGroup
import java.util.concurrent.Executors

import cats._
import cats.implicits._
import cats.effect._
import com.playerio.Connection
import fs2._
import spire.math.UByte

object ConnectionProvider {

  implicit val acg: AsynchronousChannelGroup = AsynchronousChannelGroup.withThreadPool(Executors.newCachedThreadPool())

  def socket[F[_]: ConcurrentEffect: ContextShift](endpoint: String, port: Int): Resource[F, Connection[F]] = {
    val fs2sock = io.tcp.Socket.client(new InetSocketAddress(endpoint, port))
    fs2sock.map { sock =>
      Connection(
        sock.reads(1024).chunks.map(_.map(UByte(_))),
        _.flatMap(chunk => Stream(chunk.toArray: _*)).map(_.signed).through(sock.writes())
      )
    }
  }

}
