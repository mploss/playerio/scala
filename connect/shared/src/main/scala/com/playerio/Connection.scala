package com.playerio

import cats._
import cats.implicits._
import cats.effect.{ConcurrentEffect, ContextShift, Resource}
import com.playerio.internal.ConnectionProvider
import fs2.{Chunk, Pipe, Stream}
import spire.math.UByte

case class Connection[F[_]](reads: Stream[F, Chunk[UByte]], writes: Pipe[F, Chunk[UByte], Unit]) {

  val inbound: Stream[F, WSMessage] = Stream.empty // TODO
  val outbound: Pipe[F, WSMessage, Unit] = _.drain // TODO

}

object Connection {

  def apply[F[_]: ConcurrentEffect: ContextShift](
    joinKey: String
  )(endpoint: String, port: Int): Resource[F, Connection[F]] = {
    val res: Resource[F, Connection[F]] = ConnectionProvider.socket[F](endpoint, port)
    res.evalMap { c =>
      Stream[F, UByte](UByte(0)).chunks.through(c.writes).compile.drain.as(c)
    }
  }

}
