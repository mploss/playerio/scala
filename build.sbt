import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

lazy val root = (project in file(".")).aggregate(
  playerioJS, playerioJVM,
  connectJS, connectJVM,
  protosJS, protosJVM,
  messageJS, messageJVM,
  apiJS, apiJVM,
  jLibrary,
  testProject
)

val playerioSettings = Seq(
  organization := "tf.bug",
  name         := "playerio",
  version      := "0.1.0",
  scalaVersion := "2.12.8",
  libraryDependencies ++= Seq(
    "org.typelevel" %%% "cats-core"   % "1.6.0",
    "org.typelevel" %%% "cats-effect" % "1.2.0",
    "com.pepegar" %%% "hammock-core"  % "0.9.1",
  ),
  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-feature",
    "-Ypartial-unification",
    "-language:higherKinds",
  ),
  addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.10.0"),
)

lazy val playerio =
  crossProject(JSPlatform, JVMPlatform /* , NativePlatform */ )
    .crossType(CrossType.Pure)
    .settings(playerioSettings)
    .jsSettings( /* crossScalaVersions := Seq("2.12.8") */ )
    .jvmSettings( /* crossScalaVersions := Seq("2.12.8") */ )
    .dependsOn(protos, connect, api, message)
// .nativeSettings(/* ... */)

lazy val playerioJS = playerio.js
lazy val playerioJVM = playerio.jvm

val messageSettings = Seq(
  organization := "tf.bug",
  name         := "playerio-message",
  version      := "0.1.0",
  scalaVersion := "2.12.8",
  libraryDependencies ++= Seq(
    "org.typelevel" %%% "cats-core"   % "1.6.0",
    "org.typelevel" %%% "cats-effect" % "1.2.0",
  ),
  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-feature",
    "-Ypartial-unification",
    "-language:higherKinds",
  ),
  addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.10.0"),
)

lazy val message =
  crossProject(JSPlatform, JVMPlatform /* , NativePlatform */ )
    .crossType(CrossType.Full)
    .settings(messageSettings)
    .jsSettings(libraryDependencies ++= Seq(
      "io.circe" %%% "circe-core" % "0.11.1",
      "io.circe" %%% "circe-parser" % "0.11.1",
      "io.github.scalapb-json" %%% "scalapb-circe" % "0.5.0-M3",
    ))
    .jvmSettings( /* ... */ )
    .dependsOn(protos)

lazy val messageJS = message.js
lazy val messageJVM = message.jvm

val apiSettings = Seq(
  organization := "tf.bug",
  name         := "playerio-api",
  version      := "0.1.0",
  scalaVersion := "2.12.8",
  libraryDependencies ++= Seq(
    "org.typelevel" %%% "cats-core"   % "1.6.0",
    "org.typelevel" %%% "cats-effect" % "1.2.0",
    "com.pepegar" %%% "hammock-core"  % "0.9.1",
  ),
  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-feature",
    "-Ypartial-unification",
    "-language:higherKinds",
  ),
  addCompilerPlugin("org.typelevel" %% "kind-projector" % "0.10.0"),
)

lazy val api =
  crossProject(JSPlatform, JVMPlatform /* , NativePlatform */ )
    .crossType(CrossType.Full)
    .settings(apiSettings)
    .jsSettings( /* ... */ )
    .jvmSettings(libraryDependencies += "com.pepegar" %% "hammock-apache-http" % "0.9.1")
    .dependsOn(message)

lazy val apiJS = api.js
lazy val apiJVM = api.jvm

val connectSettings = Seq(
  organization := "tf.bug",
  name := "playerio-connect",
  version := "0.1.0",
  scalaVersion := "2.12.8",
  libraryDependencies ++= Seq(
    "co.fs2" %%% "fs2-core" % "1.0.4",
    "org.typelevel" %%% "spire" % "0.16.1",
  ),
  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-feature",
    "-Ypartial-unification",
    "-language:higherKinds",
  ),
)

lazy val connect =
  crossProject(JSPlatform, JVMPlatform /* , NativePlatform */ )
    .crossType(CrossType.Full)
    .settings(connectSettings)
    .jsSettings(libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "0.9.7")
    .jvmSettings(libraryDependencies += "co.fs2" %%% "fs2-io" % "1.0.4")

lazy val connectJS = connect.js
lazy val connectJVM = connect.jvm

val protosSettings = Seq(
  organization := "tf.bug",
  name         := "playerio-protos",
  version      := "0.1.0",
  scalaVersion := "2.12.8",
  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-feature",
  ),
  PB.targets in Compile := Seq(
    scalapb.gen() -> (sourceManaged in Compile).value
  ),
  PB.protoSources in Compile := Seq((baseDirectory in ThisBuild).value / "protos" / "src" / "main" / "protobuf"),
)

lazy val protos =
  crossProject(JSPlatform, JVMPlatform /* , NativePlatform */ )
    .crossType(CrossType.Pure)
    .settings(protosSettings)
    .jsSettings( /* crossScalaVersions := Seq("2.12.8") */ )
    .jvmSettings( /* crossScalaVersions := Seq("2.12.8") */ )
// .nativeSettings(/* ... */)

lazy val protosJS = protos.js
lazy val protosJVM = protos.jvm

lazy val jLibrary = (project in file("jlib")).settings(
  organization                                := "com.playerio",
  name                                        := "playerio",
  version                                     := "3.5.4",
  autoScalaLibrary                            := false,
  libraryDependencies += "com.google.android" % "android" % "4.1.1.4",
)

lazy val testProject = (project in file("testproject")).settings(
  organization := "tf.bug",
  name := "pio-test",
  version := "0.1.0",
).dependsOn(jLibrary)
