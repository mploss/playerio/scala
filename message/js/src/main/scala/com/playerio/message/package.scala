package com.playerio

import io.circe.Json

package object message {

  type MessageData = Json

}
