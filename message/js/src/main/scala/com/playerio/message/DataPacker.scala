package com.playerio.message

import io.circe._
import io.circe.syntax._
import scalapb.{GeneratedMessage, GeneratedMessageCompanion, Message}
import scalapb_circe.JsonFormat

object DataPacker {

  def pack[I <: GeneratedMessage with Message[I]](i: I): MessageData = JsonFormat.toJson(i)

  def unpack[O <: GeneratedMessage with Message[O]](d: MessageData)(
    implicit companion: GeneratedMessageCompanion[O]
  ): O = JsonFormat.fromJson(d)

}
