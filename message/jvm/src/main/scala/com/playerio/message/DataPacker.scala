package com.playerio.message

import scalapb.{GeneratedMessage, GeneratedMessageCompanion, Message}

object DataPacker {

  def pack[I <: GeneratedMessage with Message[I]](i: I): MessageData = i.toByteArray

  def unpack[O <: GeneratedMessage with Message[O]](d: MessageData)(
    implicit companion: GeneratedMessageCompanion[O]
  ): O = companion.parseFrom(d)

}
