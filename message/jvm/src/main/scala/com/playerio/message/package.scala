package com.playerio

package object message {

  type MessageData = Array[Byte]

}
