package com.playerio.message

import com.playerio.proto.auth._
import scalapb.GeneratedMessageCompanion

trait Auth {

  implicit val authenticateMessage: PlayerIOMessage.Aux[AuthenticateArgs, AuthenticateOutput] =
    new PlayerIOMessage[AuthenticateArgs] {
      override type Output = AuthenticateOutput

      override implicit val companion: AuthenticateOutput.type = AuthenticateOutput

      override val code: Int = 13
    }

}
