package com.playerio.message

import com.playerio.proto.room.{JoinRoomArgs, JoinRoomOutput, ListRoomsArgs, ListRoomsOutput}

trait Room {

  implicit val listRoomsMessage: PlayerIOMessage.Aux[ListRoomsArgs, ListRoomsOutput] =
    new PlayerIOMessage[ListRoomsArgs] {
      override type Output = ListRoomsOutput

      override implicit val companion: ListRoomsOutput.type = ListRoomsOutput

      override val code: Int = 30
    }

  implicit val joinRoomMessage: PlayerIOMessage.Aux[JoinRoomArgs, JoinRoomOutput] = new PlayerIOMessage[JoinRoomArgs] {
    override type Output = JoinRoomOutput

    override implicit val companion: JoinRoomOutput.type = JoinRoomOutput

    override val code: Int = 24
  }

}
