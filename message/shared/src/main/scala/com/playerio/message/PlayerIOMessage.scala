package com.playerio.message

import scalapb.{GeneratedMessage, GeneratedMessageCompanion, Message}

trait PlayerIOMessage[Args <: GeneratedMessage with Message[Args]] {

  type Output <: GeneratedMessage with Message[Output]

  implicit val companion: GeneratedMessageCompanion[Output]

  def encode(a: Args): MessageData = DataPacker.pack(a)
  def decode(a: MessageData): Output = DataPacker.unpack(a)

  val code: Int

}

object PlayerIOMessage {

  type Aux[A <: GeneratedMessage with Message[A], O <: GeneratedMessage with Message[O]] = PlayerIOMessage[A] {
    type Output = O
  }

  def apply[I <: GeneratedMessage with Message[I]](implicit message: PlayerIOMessage[I]): PlayerIOMessage[I] = message

}
